import consola from 'consola'
import { chromium, firefox, webkit } from 'playwright'

const browsers = { chromium, firefox, webkit }

async function main() {
  const url = process.argv[2]
  consola.start(`Eseguo i test all'indirizzo ${url}`)

  await Promise.all(Object.entries(browsers).map(async ([name, browser]) => {
    const instance = await browser.launch()
    const context = await instance.newContext()
    const page = await context.newPage()
    consola.info(`Inizializzazione browser ${name}...`)
    await page.goto(url)
    await page.screenshot({ path: `results/example-${name}.png`, fullPage: true })
    await instance.close()
    consola.success(`Screenshot per ${name} acquisito!`)
  }))
}

main()